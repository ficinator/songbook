# Songbook

## S IQ 88
Dáša Fon Fľaša

```
G    Em
S IQ 88
Am            D
ťažký je život a ja ti nezávidím.
S IQ 88
ťažký je život naozaj si to myslím.

Am C D Am

C    D
S IQ 88
bezmocný, zmätený, nechápeš súvislostiam,
len rýchle riešenia, dlhodobé ťa zlostia.

V mysli usídli sa iracionalita,
ľudský život je pre teba banalita.

S IQ 88
stratí sa súcit, láska, tolerancia.
Ja viem, nemal by som asi,
veď to sú pre teba ťažké a cudzie slová.

S IQ 88
Odkryješ ihneď spiknutia, konšpirácie,
ľahko odhalíš žido-boľševické svine.

Vyriešiš rýchlo feťákov aj buzíkov,
ty jasne vidíš farboslepou optikou.

S IQ 88
ťažko sa žije, všetci sú proti tebe.
Z toľkej nenávisti
a paranoje ti to už pekne **be.

S IQ 88
máš strach a nenávidíš neznáme veci zvonku,
zúfalo potrebuješ nejakú tupú svorku.

Ťažko sa priznáva, ak život máš posraný,
čo iné ostáva, všetko zvaliť na slabších.

S IQ 88
nedovidíš ďalej od krčmového stolu.
S IQ 88
asi nepochopíš ani túto metaforu.

S IQ 88
ťažký je život a ja ti nezávidím.
S IQ 88
ťažký je život naozaj si to myslím.

S IQ 88
okno nenakreslíš.
S IQ 88
vždy ti vyjde hákový kríž.

Am C D Am
Hákový kríž... (Hakenkreuz)

          C                   D         G
...aj keď ty tam niečo celkom iné uvidíš.
```

## Bicykel vracia úder
Dáša Fon Fľaša

```
D                  A
Základná výbava je strohá:
E                  F#
 bicykel, klince a kukla.

Do praku náboje všetkých druhov,
odvaha, šťastie a potom let’s go.
Je to permanentná vendetta
za všetky ústrky na cestách.
Za všetky obete na dvoch kolesách
pomsta je sladká aaah, aaah.

A         F#        D          A    
Bicykloví bojovníci dostanú ťa vo dne v noci
nik nebude na pomoci jejeje,
jejejeje, jejeje...

Bicykloví bojovníci majú pamäť ako slon
na bojovom chodníku s motorovou háveďou.
Každá zlá ŠPZ-ka bude potrestaná
bicyklovým partizánom trikrát sláva, sláva, sláva!
Proti sprostým motorkám čo hučia po lesoch,
proti bezohľadným pánom vo veľkých džípoch.
Keď to nejde inak, pôjde to po zlom,
bicykel vracia úder, úder za úderom.
A jasný odkaz všetkým besným šoférom:
počúvajte chmuľovia, cesty patria bicyklom!
 
Bicykloví bojovníci dostanú ťa vo dne v noci
nik nebude na pomoci jejeje,
jejejeje, jejeje...
```

## Green Fields Of France
Dropkick Murphys

CAPO ON 1st FRET

```
   D          Bm       G        A
Oh how do you do young Willie McBride,
                               G            D
Do you mind if I sit here down by your graveside,
               Bm           G           A
And rest for a while in the warm summer sun,
                              G          D
I've been walking all day and I'm nearly done.
      D           Bm                  G    Em
And I see by your gravestone you were only 19,
         A                G         D A
When you joined the great fallen in 1916,
       D             Bm          Em
Well I hope you died quick and I hope you died clean,
   A                     G          D
Or Willie McBride was it slow and obscene.
 
        A
Did they beat the drum slowly,
        G             D
Did they play the fife lowly,
        A                              G           A
Did they sound the death march, as they lowered you down,
        G                           D   Bm
Did the band play the last post and chorus,
        D              G              A   D
Did the pipes play the Flowers of the Forest.


[Verse]

    D               Bm              G      A
And did you leave a wife or a sweetheart behind,
                            G        D
In some loyal heart is your memory enshrined,
                Bm           G A
And though you died back in 1916,
    A                        G       D
To that loyal heart  you're forever 19.
            Bm               Em
Or are you a stranger without even a name,
    A     G                 D              A
Forever enshrined behind some old glass pane,
    D           Bm         Em
In an old photograph torn tattered and stained,
    A                    G             D
And faded to yellow in a brown leather frame.


[Chorus]

        A
Did they beat the drum slowly,
        G             D
Did they play the fife lowly,
        A                              G           A
Did they sound the death march, as they lowered you down,
        G                           D   Bm
Did the band play the last post and chorus,
        D              G              A   D
Did the pipes play the Flowers of the Forest.


[Verse]

    D           Bm            G               A
The sun shining down on these green fields of France,
                                    G           D
The warm wind blows gently and the red poppies dance,
                Bm            G         A
The trenches have vanished long under the plow
                            G          D
No gas, no barbed wire, no guns firing now.
    D            Bm               G               Em
But here in this graveyard that's still "No Man's Land",
    A               G          D            A
The countless white crosses in mute witness stand,
    D           Bm              Em
To man's blind indifference to his fellow man,
    A                          G             D
And a whole generation that were butchered and damned.


[Chorus]

        A
Did they beat the drum slowly,
        G             D
Did they play the fife lowly,
        A                              G           A
Did they sound the death march, as they lowered you down,
        G                           D   Bm
Did the band play the last post and chorus,
        D              G              A   D
Did the pipes play the Flowers of the Forest.


[Verse]

    D              Bm         G        A
And I can't help but wonder, oh Willie McBride
                        G             D
Do all those who lie here know why they died,
        Bm       G                                A
Did you really believe them when they told you the cause
                                G             D
Did you really believe that this war would end wars.
                        Bm          G         Em
Well, the suffering, the sorrow, the glory, the shame
    A           G        D               A
The killing and dying it was all done in vain,
    D        Bm           Em
Oh Willie McBride it all happened again,
    A          G          D
And again, and again, and again, and again.


[Chorus]

        A
Did they beat the drum slowly,
        G             D
Did they play the fife lowly,
        A                              G           A
Did they sound the death march, as they lowered you down,
        G                           D   Bm
Did the band play the last post and chorus,
        D              G              A   D
Did the pipes play the Flowers of the Forest.
```

## Prostredsong
Saténové ruky

```
Na život by som asi volil trópy,
žijem však uprostred strednej Európy.
čo ma ešte trošku ťahá dolu
je, že ukončil som iba strednú školu.

To mi nevadilo až do momentu,
keď ma dali do stredného menežmentu.
Radosť sa nedostaví ani vtedy,
keď riadim auto nižšej strednej triedy.

A tak vlastne ani neviem kedy,
stal som sa príslušníkom strednej triedy.
A tak si vravím: chlapče, reku,
ty máš asi krízu stredného veku.

A keď príde čas posledného súdu
a prví a posledný sa hľadať budú,
zistia ako som žil svoje dni
vždy som bol aj budem prostredný.
```

## Za sklom
Korben Dallas

```
e------------------------------
h-6-10-------------6-10------6-
g------5--2-0-2---------5--0---
d------------------------------
a-5-8--------------5-8-------5-
E------6--3-0-1---------6--3---

Fľaša v okne, pohár vo vani
za rámom postavy, studenie na dlani

A všetko zlé je za sklom.

Váza s kvetmi, práčka s handrami,
na balkóne boje holubov s vranami.

A všetko zlé je za sklom.

B       Dm   C          B               
Spíme a mráz dýcha na okná,
D C
pýta sa dnu k nám.

Prach pristáva ticho na telke,
nový hrdina v špinavej prestrelke.
A všetko zlé je za sklom.

Smrť v pohľade žltej rybičky,
korunného svedka včerajšej chybičky.
A všetko zlé je za sklom.

Spíme a mráz dýcha na okná,
pýta sa dnu k nám.

Svet neprežil výbuch v zrkadle,
preč je Slavín, Hrad aj Mohyla na Bradle.
A všetko zlé je za sklom.

Mobil hladkám až po okraj,
to sklíčko dotykov a ty si taká fajn.
A všetko zlé...
Spíme a mráz dýcha na okná,
pýta sa dnu k nám.
```

## Čo sa stalo s naším životom
Slobodná Európa

```
C#mi            A#mi
Chodime znudený po uliciach
D#                G#mi
pľujeme na schody v podchodoch
nudí nás televízia
a reklamy na obchodoch

Zo strany spoločnosti
absolútna strata dôvery
lejeme do seba pivo
a nadávame na problémy

®: [F#mi]Čo sa stalo s našim ži[G#mi]votom,
[A#mi]Čo sa stalo s naším ži[C#mi]votom,
[F#mi]Čo sa asi stalo s [G#mi]naším živo[C#mi]tom[G#mi]

Dievčatá v cukrárni
sú znechutené z našich slov
a svojím výzorom
šokujeme turistov

Zbierame haliere
na drogy na pivo
na všetko kašleme
a nepočúvame rádio

®: ...

Hladní a zmrznutí
flákame sa po námestí
Revolúcia nebude
hneď dávame ruky do pestí

Zohrejeme sa pri ohni
čo večne horí pre hrdinov
zmizneme v uliciach
každý so svojou vidinou

®: ...
```

## Yetti
Peter Nagy a deti

```
Bm F# Bm F# D G A

G            D           G           D
Yetti, to je snežný muž, čo sa včera rozmrazil,
G               Bm         Em  A
všetkých vedcov ide z neho poraziť.
Prišiel k nám na anglínu, rozdával nám zmrzlinu,
zatiaľ čo ho vedci v Grónsku zháňajú.

S kamerami po meste naháňajú Yettiho
zamkni izbu, do chladničky skryme ho.
Bude celé prázdniny pripravovať zmrzliny
a chladiť pivo otcovi pod pazuchou.

   D             A      Bm         G
®: Yetti, Yetti, Yetti, studené má päty.
G         Bm          Em            A
Srdce horúce jak čaj, tiež mu svoje požičaj.
Yetti, Yetti, Yetti, má rád všetky deti
Chodí v bielom kožuchu a smeje sa juchuchú.

Babku bolia zuby a Yetti robí náladu,
prikladá jej chladný nos namiesto obkladu.
Vypli sme už kúrenie, zabudli na učenie
v obývačke urobil nám Yetti klzisko.

®: ...

Novinárom aj vedcom Yetti uteká
nechápu, že snežný muž je tiež kus človeka.
Kamery sú otrava, tak sa u nás schováva
na slávu kašle, radšej varí sladké eskimo.

®: ...
```

## Spiesser Ways to Live

```
C        F          C F
Go to sleep at nine,
       C             F               C F
with a glass of your gentle dry wine.
Choose movie night in a cozy bed,
over getting wasted in a bar with your lad.

   C        G         Am Em
®: Spiesser ways to live,
    F         D7         G       F    
so boring and weirdly attractive.
C        G       Am  	    Em
Spiesser ways to live-ive-ive,
   F           G      	C F    
so easy to get used to it.	

Have a robot vacuum clean the floor,
line up the slippers for your guests at the door.
To quench your thirst for caffeine,
own more than three types of coffee machine.

Spiesser ways to live,
so boring and quite expensive.
Spiesser ways to live-ive-ive,
so easy to get used to it.

You iron all your underwear
To concerts always bring a fisherman chair
Egg slicer is your favorite tool,
Also think that shoe spoon and back scratcher are cool

Spiesser ways to live,
so boring and very inactive.
Spiesser ways to live-ive-ive,
so easy to get used to it.

Name your plants as if they were pets,
you could also decorate them with cute little hats.
Solve a few crosswords while you poo,
Oh no! I'm out of 5-layered toilet paper, what i’m gonna do?!

Spiesser ways to live,
so boring and a bit seclusive. (seclusive yeah!)
Spiesser ways to live-ive-ive,
so easy to get used to it.

Am     G       C        Em   F     G
Carbonate your water in soda makers
F        G        C        	Em  	   F
Be the last lucky buyer of martini shakers
G                     Am
Also cut the lawn accurately with scissors
G                             Am
Always wash your lettuce in a salad spinner
G                     Am
Brew your beer and ferment your grapes
     F                               G
they may not rhyme, but they’re quite possibly...


Spiesser ways to live,
Spiesser ways to li-ive
Spiesser ways to live-ive-ive,
so easy
So easy to get used to it.

Don't forget the parasol when you go to the pool.
```

## Mamina
Toddler Punk

```
Hm A E

Hm             A      Em
  Kto k tebe vstáva v noci
kto ti pitný režim pripomína
Kto rozptýli hlúpy pocit
Mamina, mamina, mamina,
mamina, mamina, mamina
 
Kto s tebou kráča svetom
kto si už na riadny spánok nespomína
kto predstaví ťa iným deťom
Mamina, mamina, mamina,
mamina, mamina, mamina

G         Hm
 Nespí a kmitá, aj keď 
A
 výsledky v nedohľadne
             G
a krky stále hladné
         Hm
Sme trucovitá banda
A
 s veľkými očami
                   G
v ktorých je láska skrytá
 
S kým sa pri obede ráta
kto ťa vždy tak silno vyobjíma
koho mamina má rada
Tatina, tatina, tatina,
tatina, tatina, tatina
```

## Mňam
Toddler Punk

```
Em                                        D
Mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam
       G              F#       Hm          C
Úlohou mám je dať nám mňam aké mňam si vypýtam?
 
Mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam

Aj oco vie, čo chutí nám 
pozriem sa,  čo v miske mám

Em              G
Pšenko, pšenko, kaša, kaša
Em              G
Pšenko, pšenko, kaša naša
Em              G
Pšenko, pšenko, kaša, kaša
Hm      A            G
prázdny tanier, plná šálka 
Hm   A          G
na obruse veľká mláka
 
Mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam
Úlohou mám je dať nám mňam aké mňam si vypýtam?
 
Pšenko, pšenko, kaša, kaša
Pšenko, pšenko, kaša naša 
Pšenko, pšenko, kaša, kaša
prázdny tanier, plná šálka 
na obruse veľká mláka
 
Mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam, mňam
```

## Kukulienka, kde si bola

```
C           G            Am          D
Kukulienka, kde si bola, keď tá tuhá zima bola?
C       G       C          G
Ajajaj, kukuku, sedela som na buku,

hajaja, kukuku, sedela som na buku.
 
Sedela som na tom dube, kde janíčko drevo rúbe.
Hajaja, kukuku, sedela som na buku,
hajaja, kukuku, sedela som na buku

Keď som včera prala šaty, stratila som prsteň zlatý.
Hajaja, kukuku, sedela som na buku,
hajaja, kukuku, sedela som na buku.

Vrabec kričí spoza kríčka, že je prsteň u Janíčka.
Hajaja, kukuku, sedela som na buku,
hajaja, kukuku, sedela som na buku.

Sova volá spod jedličky, že je prsteň u Aničky.
Hajaja, kukuku, sedela som na buku,
hajaja, kukuku, sedela som na buku.
```

## Mandarinka Darinka
Michaela

```
C
Jeden pomaranč sa zaľúbil raz
F          Dm
do Darinky mandarinky
F           G
vedel o tom už celý strom
F        G         C
že už je na svatbu čas.

Ale niekto vtom vyšiel na strom
a obral všetky mandarinky
a poslal ich tam, vlakom až k nám
aj s mandarinkou Darinkou

Am                 Em
Išla dvadsať dní a dvadsať nocí
  F     Em     G
a stále cestou plakala
Am                 Em
od lásky odísť, to je zlý pocit
F    Em      G
bála sa toho od mala.

Ráno predavač počul tichučký plač
keď z výkladu si niekto kúpil
pre jeho decká priamo z Grécka mandarinku Darinku.

Doma detí šesť a chceli ju zjesť
keď počujú vtom tenkým hláskom:
Ahoj decká, ja som z Grécka,
som mandarinka Darinka.

Dovoľujem si pozvať aj vás
na krásnu svadbu priamo k nám
baľte kufre hneď dokiaľ je čas
môj ženích je niekde sám

Došli práve včas, keď pomaranča
chceli zobrať už do ranča
a nebol by už Darinkin muž
ale len chladený džús

Svadba bola krásna a veselá
spievali aj noví priatelia
Deti s Darinkou, aj chlapi z ranča
až ráno zaspali od tanca.

Jeden pomaranč sa zaľúbil raz
do Darinky mandarinky
vedel o tom už celý strom
že už je na svatbu čas.
```

## Do Aleluja (egocentrická koleda)
Leonard Cohen, Pavel Graus, Samo Trnka

```
C           Am
Tak po roku sú tu späť
C                 Am
Čo máš kúpiť vieš naspamäť
F                 G             C  G
Pre každého niečo, aj pre zlého uja
C          F    G
Ponožky aj šály máš
Am                F    G
Mince v penaženke preberáš
  Em               Am
A ľudí všade do aleluja

   F           Am
Alelúja, do alelúja
   F          C     G      C  Am
Alelúja, ľudí do alelu –uu-ja.

Kapra, figy, šalátik
mandarínky, banánik
Zapiť fľašou Prazdroja
To by boli krásne sviatky,
So šampanským a bez matky
Ale čo ty chudák, len starostí do alelúja.

Alelúja, Do alelúja,
Alelúja, starostí do alelu-uu-ja.

Kapustová vifonka
Vianočkou je laskonka
vianočný stromček môže byť aj tuja
Deťom v núdzi nedaj nič
len malý dvor a dlhý bič
kto kedy tebe dal čo zdarma - aleluja.

Alelúja, alelúja,
Alelúja, tak je alelu-uu- ja

Keby toto videl Ježiško
Zaklial by jak Meliško
V mestách besnie nákupná ruja
Ešte že su stánky s vínom
Vyjadrím tam sa termínom
A pošlem všetkých rovno do Alelúja

Alelúja, do Alelúja,
Alelúja, choďte do alelu-uuu- ja
```

## Vlak
Horkýže Slíže

```
Rýchlik na tretej koľaji!

F# A         E     H    E F#
  Rýchlik na tretej koľaji
     A              E       H     E F#
postojí ti v pekle, že vraj i v raji
predavačka stieracích losov
je špatná jak zubatá s kosou
Í bisťu už ti to ide
keď nechytíš tento nepôjdeš nikde
po chvíli je stanica prázdna
bez výpravcu, miestneho blázna.

F#         E
  Odchádza vlak, odchádza

D               F#
vlak z Trenčína do Šaštína
  E          F#
v kupé sedíš ty a vrah
 z Trenčína do Šaštína
v kupé sedíš len ty a vrah.

D         C#          A  F#     E  F#
 Len ty a vrah, to sú tvoje horory na videu
hľadáme strach, pochováme sladkých nocí ideu
rúti sa vlak, strach a hrôza si to brúsi po kraji
podivný hlas hlási: rýchlik na tretej koľaji!

Rýchlik na tretej koľaji
postojí ti v pekle, že vraj i v raji
démoni už ceria čeľuste
ľudia skáču z vlaku: seruste!
prrrrrr, ako na koni uzdu
čiasi ruka ťahá záchrannú brzdu
všetko uteká o preteky
všetko uteká o preteky.

Kto by chcel od upírov skrvavené cucfleky?
Kto by chcel od upírov skrvavené cucfleky?

Ref.

Rýchlik na tretej koľaji
postojí ti v pekle, že vraj i v raji
v agónii ničí vagóny
u King Konga blbnú hormóny
vŕzgajú na hajzli pánty
to Saxana s Drakulom inflagranti
sledujem ich cez malú škáru
ich technika zas nemá páru.

Na konci bola svadba pol kráľovstva do daru
tak decká do postele, vaši chcú ísť do baru.
```

## Sršeň
Horkýže Slíže

```
F#m            A          D Hm         A
  Vždy, keď sa mladý sršeň    prvýkrát osmelí
tak váha, či ju pichne alebo opelí.
Potom sa tisnú k sebe jak rosa k lupeňu
preruší ranné zore tú ich obľúbenú.

  A                F#m                 D   Hm
|: Bzzz bz-bz-bz bzzz bz-bz-bz-bz-bz bzzz, bzzz bz-bz-bz-bz-bzzz :|

Vždy, keď sa mladý chlapec vytiahne pred druhým
tak ide priamo na vec susedom za kurín.
Nachvíľu bude hviezdou z tých, ktoré vyhasnú
sršňom vypáli hniezdo a tie ho napadnú.

Ref.

Tak celý dobodaný domov sa doplazí
a okom opuchnutým pozerá cez slzy
a hňeď sa tisne k mame jak rosa k lupeňu
ona mu zabzučala jeho obľúbenú.
```
